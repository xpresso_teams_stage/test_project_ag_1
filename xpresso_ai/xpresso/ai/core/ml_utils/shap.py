import logging
import os
import random
import re
from itertools import combinations

import numpy as np
import pandas as pd
import seaborn as sns
import shap
from matplotlib import pyplot as plt, rcParams

from xpresso.ai.core.commons.utils.constants import EMPTY_STRING
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.ml_utils.generic import MOUNT_PATH, OUTPUT_PATH_ARG
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

logger = XprLogger(name="ShapUtils",
                   level=logging.INFO)
shap.initjs()

SHAP_EXPLAINER_ARG = "shap_explainer"
SHAP_EXPLAINING_SET_SIZE_ARG = "shap_explaining_set_size"
SHAP_N_INDIVIDUAL_INSTANCE_ARG = "shap_n_individual_instance"
SHAP_N_TOP_FEATURES_ARG = "shap_n_top_features"
SHAP_N_TOP_INTERACTION_FEATURES_ARG = "shap_n_top_interaction_features"
SHAP_LINK_ARG = "shap_link"
SHAP_FEATURE_NAMES_ARG = "shap_feature_names"

SHAP_EXPLAINER_DICT = {"TreeExplainer": shap.TreeExplainer,
                       "KernelExplainer": shap.KernelExplainer,
                       "DeepExplainer": shap.DeepExplainer,
                       "GradientExplainer": shap.GradientExplainer,
                       "SamplingExplainer": shap.SamplingExplainer,
                       'LinearExplainer': shap.LinearExplainer}
SHAP_FOLDER = os.path.join(MOUNT_PATH, "reported_data/explainability")


class ShapUtils:
    def __init__(self, model, explainers_list, parameters_json):
        """Args:
            parameters_json:
                shap_explainer (string, optional): choose one of
                explainer in
                 the following list or if leave as None, the method will try
                 each
                 explainer in the list in order until it reaches one that
                 that is
                 compatible with the given model type {"TreeExplainer",
                 "LinearExplainer", "DeepExplainer","GradientExplainer",
                 "KernelExplainer", "SamplingExplainer"}
                shap_feature_names (list of strings, optional) : a list of
                    column names
                shap_explaining_set_size(int, optional): Number of samples in X
                    to be explained by SHAP.
                shap_n_individual_instance (int, optional): Number of individual
                    instance(s)/sample(s)/observation(s) in the explaining
                    set to
                    be randomly chosen to create SHAP Force Plots and SHAP
                    Decision Plots.  Default is 1.
                shap_n_top_features(int, optional): Number of top features (by
                    SHAP Values) to create SHAP Dependence Plots. If values is
                    set to -1, all independent features will be used (one SHAP
                    Dependence Plot for each feature). Default is 5.
                shap_n_top_interaction_features(int, optional): Number of top
                    features (by SHAP Interaction Values) to create SHAP
                    Interaction Values Dependence Plot (only supporting
                    TreeExpaliner for now ). If values is set to -1,
                    all independent features will be used (one SHAP Interaction
                    Values Dependence Plot for each pair of features).
                    Default is 3.
                shap_link (string, optional) : default is 'identity' or use
                    'logit' to transform log odds to probabilities"""
        self.model = model
        self.explainers_list = explainers_list
        self.explainer = None
        self.params_json = self.fetch_shap_parameters(parameters_json)

    @staticmethod
    def fetch_shap_parameters(parameters_json):
        parameter_default_value = {
            SHAP_EXPLAINER_ARG: None, SHAP_EXPLAINING_SET_SIZE_ARG: None,
            SHAP_N_INDIVIDUAL_INSTANCE_ARG: 1, SHAP_N_TOP_FEATURES_ARG: 5,
            SHAP_N_TOP_INTERACTION_FEATURES_ARG: 3, SHAP_LINK_ARG: "identity",
            SHAP_FEATURE_NAMES_ARG: None, OUTPUT_PATH_ARG: SHAP_FOLDER}
        fetched_parameters = dict()
        for parameter, default_value in parameter_default_value.items():
            fetched_parameters[parameter] = parameters_json.get(
                parameter, default_value)
        return fetched_parameters

    def explain_shap(self, x_values):
        """
            Create Shapley plots for the trained model.
            Allows an approximation of shapley values for a given model based on
            a number of samples from data source X
            Args:
                x_values (dataframe, numpy array, or pixel_values) : dataset to
                    be explained
            Returns:
                Returns: SHAP plots
            """
        output_path = self.params_json.get(OUTPUT_PATH_ARG)
        print(
            f"Generating SHAP explainability plots. Output Path: {output_path}")

        # generate explainer based on model type
        self.explainer = self.generate_explainer(
            x_values, self.params_json.get(SHAP_EXPLAINER_ARG))

        # end the program if the explainer is None
        if not self.explainer:
            logger.exception("Unable to generate SHAP explainer")
            return

        # print the type of SHAP explainer used if explainer generated
        # successfully
        print("SHAP Explainer used : ", self.return_name(self.explainer))

        # assign variables
        shap_n_top_features = x_values.shape[1] if self.params_json.get(
            SHAP_N_TOP_FEATURES_ARG) == -1 else None
        # define number of top features to plot interaction values plots
        shap_n_top_interaction_features = x_values.shape[1] if \
            self.params_json.get(SHAP_N_TOP_INTERACTION_FEATURES_ARG) == -1 \
            else None
        shap_link = self.params_json.get(SHAP_LINK_ARG)
        shap_n_individual_instance = self.params_json.get(
            SHAP_N_INDIVIDUAL_INSTANCE_ARG)
        x_explain = self.generate_explaining_set(
            x_values, self.params_json.get(SHAP_EXPLAINING_SET_SIZE_ARG))
        # compute SHAP value, expected value, interaction values(if support)
        shap_values = self.explainer.shap_values(x_explain)
        expected_value = self.explainer.expected_value
        try:
            shap_interaction_values = self.explainer.shap_interaction_values(
                x_explain)
        except Exception:
            shap_interaction_values = None

        # obtain feature names
        feature_names = self.shap_generate_feature_names(
            x_values, self.params_json.get(SHAP_FEATURE_NAMES_ARG))

        # check if shap_values have multiple elements, as a result of a
        # multioutput model
        if not isinstance(shap_values, list):
            multioutput_n = EMPTY_STRING
            self.shap_plots_single_output(
                shap_values=shap_values, expected_value=expected_value,
                shap_interaction_values=shap_interaction_values,
                x_explain=x_explain, feature_names=feature_names,
                multioutput_n=multioutput_n,
                link=shap_link,
                shap_n_top_features=shap_n_top_features,
                shap_n_individual_instance=shap_n_individual_instance,
                shap_n_top_interaction_features=shap_n_top_interaction_features,
                output_path=output_path)
            return

        for index, shap_value in enumerate(shap_values):
            # provide a label for the output
            multioutput_n = f"multioutput_{index}"
            shap_interaction_values_i = None
            if shap_interaction_values:
                shap_interaction_values_i = shap_interaction_values[index]
            self.shap_plots_single_output(
                shap_values=shap_value, expected_value=expected_value[index],
                shap_interaction_values=shap_interaction_values_i,
                x_explain=x_explain, feature_names=feature_names,
                multioutput_n=multioutput_n, link=shap_link,
                shap_n_top_features=shap_n_top_features,
                shap_n_individual_instance=shap_n_individual_instance,
                shap_n_top_interaction_features=shap_n_top_interaction_features,
                output_path=output_path)

    def generate_explaining_set(self, x_values, shap_explaining_set_size):
        """Generate SHAP explaining set"""
        # if user provides number of data to explain
        if shap_explaining_set_size:
            n_explain = shap_explaining_set_size
        # For Kernel explainer, use up to 1000 samples to speed up the
        # process as
        elif isinstance(self.explainer, shap.KernelExplainer):
            n_explain = min(1000, x_values.shape[0])
        else:
            n_explain = x_values.shape[0]

        # prepare data
        if isinstance(x_values, pd.DataFrame):
            x_values = x_values.to_numpy()

        # obtain explaining set (x matrix to be explained by SHAP)
        x_explain_ind = np.random.choice(
            x_values.shape[0], min(n_explain, x_values.shape[0]), replace=False)
        return x_values[x_explain_ind]

    def generate_explainer(self, x_values, shap_explainer):
        """Generate SHAP explainer"""
        # convert background data as numpy array,
        # Based on SHAP, anywhere from 100 to 1000 random background samples
        # are good sizes to use
        if isinstance(x_values, pd.DataFrame):
            x_values = x_values.to_numpy()
        bdata = x_values[
            np.random.choice(x_values.shape[0], min(1000, x_values.shape[0]),
                             replace=False)]
        # if user choose a SHAPExplainer, go with user's preference
        explainer = self.get_shap_explainer(bdata, shap_explainer)

        # if user doesn't provide a SHAPExplainer of choice or the
        # SHAPExplainer provided by user is not compatible with
        # the model, try other SHAP explainers in the order of the list
        for exp in self.explainers_list:
            if explainer:
                break
            explainer = self.get_shap_explainer(bdata, exp)
        return explainer

    def get_shap_explainer(self, bdata, shap_explainer,
                           feature_perturbation="interventional"):
        if not self.model:
            logger.exception("Model not found for generaing SHAP explainer")
            return
        if not shap_explainer:
            return None
        explainer = None
        try:
            if shap_explainer == "KernelExplainer":
                # The size of background data limited to 100 for KernelExplainer
                bdata = bdata[:100]
                explainer = SHAP_EXPLAINER_DICT[shap_explainer](
                    self.model.predict, bdata)
                return explainer
            # For SHAP TreeExplainer and feature_perturbation=interventional,
            # background data is required
            elif shap_explainer == "TreeExplainer" and feature_perturbation \
                in ["interventional", "tree_path_dependent"]:
                data = None
                if feature_perturbation != "interventional":
                    data = bdata
                explainer = SHAP_EXPLAINER_DICT[shap_explainer](
                    self.model, data, feature_perturbation=feature_perturbation)
                return explainer
            elif shap_explainer == "SamplingExplainer":
                explainer = SHAP_EXPLAINER_DICT[shap_explainer](
                    self.model.predict, bdata)
                return explainer
            else:
                explainer = SHAP_EXPLAINER_DICT[shap_explainer](
                    self.model, bdata)
                return explainer
        except Exception as exp:
            logger.exception(f"Reason: {str(exp)}")
        return explainer

    @staticmethod
    def shap_waterfall_plots(expected_value, feature_names,
                             multioutput_n,
                             shap_values, x_explain, random_observations,
                             output_path=SHAP_FOLDER):
        """SHAP waterfall plots"""
        # make sure expected value is a float value instead of a array
        # containing the float value
        try:
            expected_value = expected_value[0]
        except IndexError:
            pass

        # plot the waterfall plots for the random selected observations
        for observation in random_observations:
            try:
                shap.waterfall_plot(expected_value, shap_values[observation],
                                    x_explain[observation],
                                    feature_names=feature_names,
                                    show=False)
                plt.title(
                    f"SHAP Waterfall Plot of observation {observation}"
                    f" {multioutput_n}")
                xpresso_save_plot(
                    f"shap_waterfall_plot_sample#{observation}{multioutput_n}",
                    output_path=output_path)
            except Exception:
                logger.info("Unable to plot SHAP Waterfall plot")

    @staticmethod
    def shap_dependence_plot(feature_names, multioutput_n, shap_values,
                             x_explain, shap_n_top_features,
                             output_path=SHAP_FOLDER):
        """dependence_plot for top n most important features"""
        rank_inds = np.argsort(-np.sum(np.abs(shap_values), 0))
        top_inds = rank_inds[:shap_n_top_features]
        for i in top_inds:
            try:
                shap.dependence_plot(feature_names[i], shap_values, x_explain,
                                     feature_names=feature_names, show=False)
                plt.title(
                    f"Dependence Plot : {feature_names[i]} {multioutput_n}")
                xpresso_save_plot(
                    f"shap_dependence_plot_{feature_names[i]}{multioutput_n}",
                    output_path=output_path)
            except Exception:
                logger.info("Unable to plot SHAP Dependence plot")

    @staticmethod
    def shap_dependence_plot_interaction(feature_names, multioutput_n,
                                         shap_interaction_values, x_explain,
                                         shap_n_top_interaction_features,
                                         output_path=SHAP_FOLDER):
        """SHAP dependence plot with interaction values"""
        # sort features based on interaction values(absolute)
        tmp_abs = np.abs(shap_interaction_values).sum(0)
        for i in range(tmp_abs.shape[0]):
            tmp_abs[i, i] = 0
        sorted_inds = np.argsort(-tmp_abs.sum(0))
        sorted_features_inter = [feature_names[ind] for ind in sorted_inds]

        # Plot n top features interaction values dependence plot, default is 5
        top_features_inter = sorted_features_inter[
                             0:shap_n_top_interaction_features]
        top_feature_combs = []
        for combi in combinations(top_features_inter, 2):
            top_feature_combs.append(combi)
        for i in range(len(top_feature_combs)):
            try:
                fea_0 = top_feature_combs[i][0]
                fea_1 = top_feature_combs[i][1]
                # make plots
                shap.dependence_plot((fea_0, fea_1), shap_interaction_values,
                                     x_explain, feature_names=feature_names,
                                     show=False)
                plt.title(
                    f"Dependence Plot Interaction Values: {fea_0} vs {fea_1}")
                xpresso_save_plot(
                    f"shap_dependence_plot_{fea_0}_vs_{fea_1}{multioutput_n}",
                    output_path=output_path)
            except Exception:
                logger.info("Unable to plot SHAP Dependence plot interaction")

    @staticmethod
    def shap_force_plot_collective(expected_value, feature_names, link,
                                   multioutput_n, shap_values, x_explain,
                                   output_path=SHAP_FOLDER):
        """SHAP force plot collective"""
        try:
            rcParams['axes.titlepad'] = 24
            force_plot = shap.force_plot(expected_value, shap_values, x_explain,
                                         feature_names=feature_names, link=link)
            shap.save_html(
                os.path.join(output_path, f"shap_force_plot(collective)"
                                          f"{multioutput_n}.html"),
                force_plot)
        except Exception as e:
            logger.info("Unable to plot SHAP force plot")

    @staticmethod
    def shap_inter_values_heatmap(feature_names, multioutput_n,
                                  shap_interaction_values,
                                  shap_n_top_features,
                                  output_path=SHAP_FOLDER):
        """Heatmap by SHAP Interaction Values"""
        try:
            # sort features based on interaction values(absolute)
            tmp_abs = np.abs(shap_interaction_values).sum(0)
            for i in range(tmp_abs.shape[0]):
                tmp_abs[i, i] = 0
            sorted_inds = np.argsort(-tmp_abs.sum(0))
            # sum interaction values across all samples for each feature
            tmp = shap_interaction_values.sum(0)
            # re-arrange heatmap feature based on interaction values (
            # absoluate value) use top 20 features
            tmp_inds = sorted_inds[0: max(20, shap_n_top_features)]
            tmp_ranked = tmp[tmp_inds, :][:, tmp_inds]
            tem_feature_inter = [feature_names[ind] for ind in tmp_inds]

            # plot heatmap
            plt.figure(figsize=(12, 12))
            heat_map = sns.heatmap(tmp_ranked, annot=True, cmap="YlGnBu")
            plt.ylabel('Features')
            heat_map.set_yticklabels(tem_feature_inter, rotation=50.4,
                                     horizontalalignment="right")
            heat_map.set_xticklabels(tem_feature_inter, rotation=50.4,
                                     horizontalalignment="left")
            plt.title(f"Heatmap by SHAP Interaction Values {multioutput_n}")
            plt.gca().xaxis.tick_top()
            xpresso_save_plot(
                f"shap_heatmap_by_interaction_values{multioutput_n}",
                output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP inter values heatmap")

    @staticmethod
    def shap_summary_plot_bar(feature_names, multioutput_n, shap_values,
                              x_explain, output_path=SHAP_FOLDER):
        """Summary Plot with SHAP mean absolute Values (bar plot)"""
        try:
            shap.summary_plot(
                shap_values, x_explain, feature_names=feature_names,
                plot_type="bar", show=False)
            plt.title(f"Feature Importance by SHAP Values(mean absolute value) "
                      f"{multioutput_n}")
            xpresso_save_plot(f"shap_values_summary_bar_plot{multioutput_n}",
                              output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP summary bar plot")

    @staticmethod
    def shap_summary_plot_interaction(feature_names, multioutput_n,
                                      shap_interaction_values, x_explain,
                                      output_path=SHAP_FOLDER):
        """SHAP summary plot with interaction values"""
        try:
            shap.summary_plot(
                shap_interaction_values, x_explain, plot_type="compact_dot",
                feature_names=feature_names, show=False)
            plt.title(f"SHAP Interaction Value Summary Plot {multioutput_n}")
            xpresso_save_plot(
                f"shap_interaction_values_summary_plot{multioutput_n}",
                output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP summary plot interaction")

    @staticmethod
    def shap_decision_plot_interaction(shap_values, expected_value,
                                       feature_names, link, multioutput_n,
                                       shap_interaction_values, x_explain,
                                       output_path=SHAP_FOLDER):
        """SHAP interaction values decision plot"""
        # use up to 200 samples for better visualization
        sm_x_explain = min(200, len(shap_values))

        if not shap_interaction_values:
            return
        try:
            shap.decision_plot(
                expected_value, shap_interaction_values[0:sm_x_explain],
                x_explain[0:sm_x_explain], feature_names=feature_names,
                link=link, show=False)
            plt.title(f"SHAP Interaction Values Decision Plot (collective) "
                      f"{multioutput_n}")
            xpresso_save_plot(
                f"shap_interaction_values_decision_plot_collective"
                f"{multioutput_n}", output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP shap_decision_plot_interaction")

    @staticmethod
    def shap_decision_plot_single_instance(expected_value, feature_names,
                                           link, multioutput_n,
                                           shap_values, x_explain,
                                           random_observation,
                                           output_path=SHAP_FOLDER):
        """SHAP decision plot single instance"""
        # Individual Decision Plot
        for observation in random_observation:
            try:
                # plot the SHAP values for the randomly selected observations
                shap.decision_plot(
                    expected_value, shap_values[observation], x_explain[
                        observation], feature_names=feature_names, link=link,
                    show=False)
                plt.title(
                    f"SHAP Decision Plot of observation {observation}"
                    f" {multioutput_n}")
                xpresso_save_plot(
                    f"shap_decision_plot_sample#{observation}{multioutput_n}",
                    output_path=output_path)
            except Exception:
                logger.info(
                    "Unable to plot SHAP shap_decision_plot_single_instance")

    @staticmethod
    def shap_decision_plot_collective(expected_value, feature_names, link,
                                      multioutput_n, shap_values, x_explain,
                                      output_path=SHAP_FOLDER):
        """SHAP decision plot collective"""
        # For better visualization, limit to 200 samples, otherwise SHAP
        # gives a warning
        sm_x_explain = min(200, len(shap_values))
        try:
            rcParams['axes.titlepad'] = 24
            shap.decision_plot(
                expected_value, shap_values[0:sm_x_explain], x_explain[
                                                             0:sm_x_explain],
                feature_names=feature_names, link=link, return_objects=True,
                show=False)
            plt.title(f"SHAP Decision Plot (collective) {multioutput_n}")
            xpresso_save_plot(
                f"shap_decision_plot_collective{multioutput_n}",
                output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP shap_decision_plot_collective")

    @staticmethod
    def shap_force_plot_single_instance(expected_value, feature_names,
                                        link, multioutput_n, shap_values,
                                        x_explain,
                                        shap_n_individual_instance,
                                        random_observation,
                                        output_path=SHAP_FOLDER):
        """SHAP force plot single instance"""
        # Individual force plot
        for observation in random_observation:
            try:
                # plot the SHAP values for the random sampled observations
                rcParams['axes.titlepad'] = 24
                ind_force_plot = shap.force_plot(
                    expected_value, shap_values[observation],
                    x_explain[observation], feature_names=feature_names,
                    link=link)
                shap.save_html(os.path.join(
                    output_path,
                    f"shap_force_plot_sample#{observation}"
                    f"{multioutput_n}.html"),
                    ind_force_plot)
            except Exception:
                logger.info(
                    "Unable to plot SHAP shap_force_plot_single_instance")

    @staticmethod
    def shap_summary_plot_dot(feature_names, multioutput_n, shap_values,
                              x_explain, output_path=SHAP_FOLDER):
        """Summary Plot with SHAP Values (dot plot)"""
        try:
            shap.summary_plot(shap_values, x_explain,
                              feature_names=feature_names, show=False)
            plt.title(f"Feature Importance by SHAP Values {multioutput_n}")
            plt.ylabel("Features")
            xpresso_save_plot(f"shap_values_summary_plot{multioutput_n}",
                              output_path=output_path)
        except Exception:
            logger.info("Unable to plot SHAP shap_summary_plot_dot")

    @staticmethod
    def save_shap_values_df(feature_names, multioutput_n, shap_values,
                            output_path=SHAP_FOLDER):
        """Save features and shap values as dataframe in csv file"""
        # Rank features based on SHAP Values (sum of the absolute value of
        # shap values)
        shap_values_abs = (np.abs(shap_values)).sum(axis=0)
        sorted_features = [f for _, f in
                           sorted(zip(shap_values_abs, feature_names),
                                  reverse=True)]
        sorted_shap_values = [round(v, 4) for v, _ in
                              sorted(zip(shap_values_abs, feature_names),
                                     reverse=True)]
        # save SHAP values to a dataframe
        try:
            shap_values_df = pd.DataFrame({"features": sorted_features,
                                           "SHAP_values": sorted_shap_values})
            csv_file_name = os.path.join(output_path,
                                         f"shap_values{multioutput_n}.csv")
            shap_values_df.to_csv(csv_file_name)
        except Exception:
            logger.info("Unable to plot SHAP save_shap_values_df")

    def shap_plots_single_output(self, shap_values, expected_value,
                                 shap_interaction_values, x_explain,
                                 feature_names, multioutput_n, link,
                                 shap_n_top_features,
                                 shap_n_individual_instance,
                                 shap_n_top_interaction_features,
                                 output_path=SHAP_FOLDER):
        """Generates all SHAP plots"""
        # Random Sample observations
        random_observation = random.sample(range(len(x_explain)),
                                           shap_n_individual_instance)
        self.save_shap_values_df(
            feature_names, multioutput_n, shap_values, output_path)
        self.shap_summary_plot_dot(
            feature_names, multioutput_n, shap_values, x_explain,
            output_path)
        self.shap_summary_plot_bar(
            feature_names, multioutput_n, shap_values, x_explain,
            output_path)
        self.shap_dependence_plot(
            feature_names, multioutput_n, shap_values, x_explain,
            shap_n_top_features, output_path)
        self.shap_force_plot_collective(
            expected_value, feature_names, link, multioutput_n, shap_values,
            x_explain, output_path)
        self.shap_force_plot_single_instance(
            expected_value, feature_names, link, multioutput_n, shap_values,
            x_explain, shap_n_individual_instance, random_observation,
            output_path)
        self.shap_decision_plot_collective(
            expected_value, feature_names, link, multioutput_n, shap_values,
            x_explain, output_path)
        self.shap_decision_plot_single_instance(
            expected_value, feature_names, link, multioutput_n, shap_values,
            x_explain, random_observation, output_path)
        self.shap_waterfall_plots(
            expected_value, feature_names, multioutput_n, shap_values,
            x_explain, random_observation, output_path)
        # if SHAP interaction values are not available
        if not shap_interaction_values:
            return
        self.shap_summary_plot_interaction(
            feature_names, multioutput_n, shap_interaction_values, x_explain,
            output_path)
        self.shap_inter_values_heatmap(
            feature_names, multioutput_n, shap_interaction_values,
            shap_n_top_features, output_path)
        self.shap_dependence_plot_interaction(
            feature_names, multioutput_n, shap_interaction_values, x_explain,
            shap_n_top_interaction_features, output_path)
        self.shap_decision_plot_interaction(
            shap_values, expected_value, feature_names, link, multioutput_n,
            shap_interaction_values, x_explain, output_path)

    @staticmethod
    def return_name(shap_explainer_obj):
        """Return shap explainer name
        Args:
            shap_explainer_obj: SHAP explainer object
        """
        type_str = re.search(r"<class '(.*)'>", str(type(shap_explainer_obj)))
        return type_str.group(1)

    @staticmethod
    def shap_generate_feature_names(x_values, shap_feature_names):
        """Generate SHAP feature names"""
        # Define feature names
        if shap_feature_names:
            # if user provide feature names, apply user's inputs
            feature_names = shap_feature_names
        # if X is a dataframe, get feature names from dataframe
        elif isinstance(x_values, pd.DataFrame):
            feature_names = x_values.columns.values.tolist()
        # else, label feature with index
        else:
            feature_names = ["Feature_{}".format(i) for i in
                             range(x_values.shape[1])]
        return feature_names

    @staticmethod
    def shap_plots_image(shap_values, x_explain):
        """Generate SHAP image plot"""
        try:
            # plot the feature attributions
            shap.image_plot(shap_values, -x_explain)
        except Exception as exp:
            logger.exception(f"Unable to plot image_plot. Reason:{str(exp)}")
