import itertools
import os
import pickle
from itertools import cycle

import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import sklearn
from sklearn.base import BaseEstimator, is_regressor, is_classifier
from sklearn.exceptions import ConvergenceWarning
from sklearn.metrics import mean_absolute_error, mean_squared_error, \
    mean_squared_log_error, median_absolute_error, explained_variance_score, \
    max_error, r2_score, mean_poisson_deviance, mean_gamma_deviance, \
    mean_tweedie_deviance, accuracy_score, balanced_accuracy_score, \
    recall_score, precision_score, f1_score, zero_one_loss, hamming_loss, \
    jaccard_score, matthews_corrcoef, roc_auc_score, average_precision_score, \
    brier_score_loss, coverage_error, label_ranking_average_precision_score, \
    label_ranking_loss, ndcg_score, adjusted_mutual_info_score, \
    adjusted_rand_score, completeness_score, fowlkes_mallows_score, \
    homogeneity_score, mutual_info_score, normalized_mutual_info_score, \
    v_measure_score, calinski_harabasz_score, davies_bouldin_score, \
    silhouette_score, precision_recall_curve, multilabel_confusion_matrix, \
    classification_report, roc_curve, auc, confusion_matrix, hinge_loss, \
    log_loss
from sklearn.model_selection import train_test_split, validation_curve
from sklearn.preprocessing import label_binarize
from sklearn.utils import multiclass
from sklearn.utils._testing import ignore_warnings
from sklearn.utils.multiclass import unique_labels
from xpresso.ai.core.commons.utils.constants import RANDOM_STATE, \
    DEFAULT_VALIDATION_SIZE, EMPTY_STRING, DEFAULT_PREDICTION_THRESHOLD
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.ml_utils.generic import format_metric_key, \
    false_negative_rate, false_positive_rate, true_negative_rate, REGRESSOR, \
    CLASSIFIER, CLASSIFIER_TARGET_TYPES, UNSUPERVISED_CLUSTERING, BINARY, \
    MULTI_INDICATOR, PLOTS_FOLDER, METRICS_FOLDER, MODEL_TYPE_ARG, \
    VALIDATION_SIZE_ARG, GENERATE_VALIDATION_METRICS_ARG, \
    PREDICTION_THRESHOLD_ARG
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot

SKLEARN_REGRESSOR_METRICS = [
    mean_absolute_error, mean_squared_error, mean_squared_log_error,
    median_absolute_error, explained_variance_score, max_error, r2_score,
    mean_poisson_deviance, mean_gamma_deviance, mean_tweedie_deviance]
SKLEARN_BINARY_CLASSIFIER_METRICS = [
    accuracy_score, balanced_accuracy_score, recall_score, precision_score,
    f1_score, zero_one_loss, hamming_loss, jaccard_score, matthews_corrcoef]
SKLEARN_CLASSIFIER_PROBABILITY_METRICS = [
    roc_auc_score, average_precision_score, brier_score_loss]
SKLEARN_MULTICLASS_CLASSIFIER_METRICS = [
    accuracy_score, balanced_accuracy_score, zero_one_loss, hamming_loss,
    matthews_corrcoef]
SKLEARN_MULTICLASS_AVERAGING_METRICS = [
    f1_score, recall_score, precision_score, jaccard_score]
SKLEARN_MULTILABEL_CLASSIFIER = [
    accuracy_score, zero_one_loss, hamming_loss]
SKLEARN_MULTILABEL_AVERAGING_METRICS = [
    f1_score, recall_score, precision_score, jaccard_score,
    average_precision_score]
SKLEARN_MULTILABEL_RANKING_METRICS = [
    coverage_error, label_ranking_average_precision_score,
    label_ranking_loss, ndcg_score]
SKLEARN_UNSUPERVISED_CLUSTERING_METRICS = [
    adjusted_mutual_info_score, adjusted_rand_score, completeness_score,
    fowlkes_mallows_score, homogeneity_score, mutual_info_score,
    normalized_mutual_info_score, v_measure_score]
SKLEARN_CLUSTERING_SPECIFIC_METRICS = [
    calinski_harabasz_score, davies_bouldin_score, silhouette_score]


class SklearnPlots:
    def __init__(self, model, dataset_name):
        self.model = model
        self.model_classes = None
        self.dataset_name = dataset_name
        self.logger = XprLogger(name="SklearnPlots")
        # Create directory for plots if doesn't exist
        os.makedirs(PLOTS_FOLDER, exist_ok=True)

    def set_model_classes(self, y_true):
        try:
            self.model_classes = self.model.classes_
        except AttributeError:
            self.model_classes = unique_labels(y_true)

    def plot_precision_recall_curve_multi(self, y_true, y_pred_proba):
        """
        Plot precision recall curve for multiclass and multilabel classifiers
        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()
            y_true_binarized = label_binarize(
                y_true, classes=self.model_classes)
            for index, model_class in enumerate(self.model_classes):
                precision[index], recall[index], _ = precision_recall_curve(
                    y_true_binarized[:, index], y_pred_proba[:, index])
                average_precision[index] = average_precision_score(
                    y_true_binarized[:, index], y_pred_proba[:, index])
            # A "micro-average": quantifying score on all classes jointly
            precision["micro"], recall["micro"], _ = precision_recall_curve(
                y_true_binarized.ravel(), y_pred_proba.ravel())
            average_precision["micro"] = average_precision_score(
                y_true_binarized, y_pred_proba,
                average="micro")

            # Plot all precision_recall curves
            plt.figure(figsize=(10, 6))
            colors = cycle(list(mcolors.TABLEAU_COLORS.values()))
            lines = []
            labels = []
            l, = plt.plot(recall["micro"], precision["micro"], color='gold',
                          lw=2)
            lines.append(l)
            labels.append('micro-average Precision-recall (area = {0:0.2f})'
                          ''.format(average_precision["micro"]))
            for i, color in zip(range(len(self.model_classes)), colors):
                l, = plt.plot(recall[i], precision[i], color=color, lw=2)
                lines.append(l)
                labels.append('Precision-recall for class {0} (area = {1:0.2f})'
                              ''.format(i, average_precision[i]))
            fig = plt.gcf()
            fig.subplots_adjust(bottom=0.25)
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.title(
                'Precision-Recall curve' + self.dataset_name +
                ': micro AP={0:0.2f}'.format(average_precision["micro"]))
            plt.legend(lines, labels, loc="lower left", prop=dict(size=9))
            xpresso_save_plot(f"Precision_recall_curve{self.dataset_name}",
                              output_path=PLOTS_FOLDER)
        except Exception as exp:
            self.logger.exception(f"Unable to plot "
                                  f"precision_recall_curve_multi {str(exp)}")

    def plot_multilabel_confusion_matrix(self, y_true, y_pred):
        """ Plot confusion matrix for multilabel classifiers"""
        try:
            cm_plot_title = EMPTY_STRING
            # generate multilabel_confusion_matrix
            cm_arrays = multilabel_confusion_matrix(y_true, y_pred)
            fig, axes = plt.subplots(len(cm_arrays), figsize=(6, 10))

            # plot a confusion matrix for each class
            for index, element in enumerate(cm_arrays):
                df_cm = pd.DataFrame(element)
                sns.set(font_scale=1.2)  # for label size
                sns.heatmap(df_cm, annot=True,
                            cmap="Blues", ax=axes[index], fmt='g')
                cm_plot_title = format_metric_key(
                    multilabel_confusion_matrix) + \
                                self.dataset_name + "_class_" + \
                                str(self.model_classes[index])
                axes[index].set_xlabel('Predicted label')
                axes[index].set_ylabel('True label')
                axes[index].set_title(cm_plot_title)
            fig.tight_layout(pad=1.0)
            xpresso_save_plot(cm_plot_title, output_path=PLOTS_FOLDER)
            self.logger.info("Multilabel Confusion Matrix Saved")
        except Exception as exp:
            self.logger.exception(
                f"Unable to plot multilabel confusion matrix. "
                f"Reason: {exp}")

    def plot_roc_curve_multi(self, y_true, y_pred):
        """
        Plot ROC curve and compute ROC area for multiclass and multilabel
        classifiers
        """
        if y_pred is None:
            return
        try:
            y_true_binarized = label_binarize(
                y_true, classes=self.model_classes)
            fpr = dict()
            tpr = dict()
            roc_auc = dict()
            for index, element in enumerate(self.model_classes):
                fpr[index], tpr[index], _ = roc_curve(
                    y_true_binarized[:, index], y_pred[:, index])
                roc_auc[index] = auc(fpr[index], tpr[index])

            # Compute micro-average ROC curve and ROC area
            fpr["micro"], tpr["micro"], _ = roc_curve(
                y_true_binarized.ravel(), y_pred.ravel())
            roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
            # First aggregate all false positive rates
            all_fpr = np.unique(
                np.concatenate(
                    [fpr[i] for i, _ in enumerate(self.model_classes)]))
            # Then interpolate all ROC curves at this points
            mean_tpr = np.zeros_like(all_fpr)
            for index, model_class in enumerate(self.model_classes):
                mean_tpr += np.interp(all_fpr, fpr[index], tpr[index])
            # Finally average it and compute AUC
            mean_tpr /= len(self.model_classes)
            fpr["macro"] = all_fpr
            tpr["macro"] = mean_tpr
            roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
            # Plot all ROC curves
            plt.figure(figsize=(12, 6))
            line_width = 2
            plt.plot(fpr["micro"], tpr["micro"],
                     label='micro-average ROC curve (area = {0:0.2f})'
                           ''.format(roc_auc["micro"]),
                     color='deeppink', linestyle=':', linewidth=4)
            plt.plot(fpr["macro"], tpr["macro"],
                     label='macro-average ROC curve (area = {0:0.2f})'
                           ''.format(roc_auc["macro"]),
                     color='navy', linestyle=':', linewidth=4)
            colors = cycle(list(mcolors.TABLEAU_COLORS.values()))
            for i, color in zip(range(len(self.model_classes)), colors):
                plt.plot(fpr[i], tpr[i], color=color, lw=line_width,
                         label='ROC curve of class {0} (area = {1:0.2f})'
                               ''.format(i, roc_auc[i]))
            plt.plot([0, 1], [0, 1], 'k--', lw=line_width)
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('False Positive Rate')
            plt.ylabel('True Positive Rate')
            roc_plot_title = f'ROC_curve_{self.dataset_name}'
            plt.title(roc_plot_title)
            plt.legend(loc="lower right")
            xpresso_save_plot(roc_plot_title, output_path=PLOTS_FOLDER)
            self.logger.info("Multiclass/Multilable ROC Curve Saved")
        except Exception as exp:
            self.logger.exception(
                f"Unable to plot ROC curve multi. Reason:{exp}")

    def plot_roc_curve_binary(self, y_true, y_pred_proba):
        """
        Plot ROC curve and compute ROC area for binary classifiers
        """
        try:
            fpr = dict()
            tpr = dict()
            roc_auc = dict()
            index = 1
            average_roc_auc_score = roc_auc_score(y_true,
                                                  y_pred_proba[:, index])
            fpr[index], tpr[index], _ = roc_curve(
                y_true, y_pred_proba[:, index])
            roc_auc[index] = auc(fpr[index], tpr[index])
            plt.figure()
            lw = 1
            plt.plot(fpr[lw], tpr[lw], color='darkorange',
                     lw=lw, label='ROC curve (area = %0.2f)' % roc_auc[lw])
            plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
            plt.xlim([0.0, 1.0])
            plt.ylim([0.0, 1.05])
            plt.xlabel('False Positive Rate')
            plt.ylabel('True Positive Rate')
            plt.title('ROC Curve' + self.dataset_name +
                      ' : AUC ROC={0:0.2f}'.format(average_roc_auc_score))
            plt.legend(loc="lower right")
            xpresso_save_plot("ROC_curve", output_path=PLOTS_FOLDER)
        except Exception as exp:
            self.logger.exception(
                f"Unable to plot roc_curve_binary. Reason: {exp}")

    def plot_precision_recall_curve_binary(self, y_true, y_pred_proba):
        """
        Plot precision recall curve for binary classifiers
        """
        try:
            precision = dict()
            recall = dict()
            average_precision = dict()
            i = 1
            precision[i], recall[i], _ = precision_recall_curve(
                y_true, y_pred_proba[:, 1])
            average_precision[i] = average_precision_score(
                y_true, y_pred_proba[:, 1])
            plt.figure()
            plt.step(recall[i], precision[i], where='post')
            plt.xlabel('Recall')
            plt.ylabel('Precision')
            plt.ylim([0.0, 1.05])
            plt.xlim([0.0, 1.0])
            plt.title(
                'Precision-Recall Curve' + self.dataset_name +
                ': Average Precision={0:0.2f}'.format(average_precision[i]))
            xpresso_save_plot(f"Precision_recall_curve{self.dataset_name}",
                              output_path=PLOTS_FOLDER)
        except Exception as exp:
            self.logger.exception(
                f"Unable to plot precision_recall_curve. Reason: {str(exp)}")

    def plot_confusion_matrix(self, y_true, y_pred):
        """
        Plot confusion matrix for binary or multiclass classifiers
        """
        try:
            cm = confusion_matrix(y_true, y_pred)
            accuracy = np.trace(cm) / float(np.sum(cm))
            cmap = plt.get_cmap('Blues')

            plt.figure(figsize=(8, 6))
            plt.imshow(cm, interpolation='nearest', cmap=cmap)
            plt.colorbar()
            if self.model_classes is not None:
                tick_marks = np.arange(len(self.model_classes))
                plt.xticks(tick_marks, self.model_classes)
                plt.yticks(tick_marks, self.model_classes)
            thresh = cm.max() / 2
            for i, j in itertools.product(range(cm.shape[0]),
                                          range(cm.shape[1])):
                plt.text(j, i, "{:,}".format(cm[i, j]),
                         horizontalalignment="center",
                         color="white" if cm[i, j] > thresh else "black")
            plt.tight_layout()
            plt.ylabel('True label')
            plt.xlabel('Predicted label\naccuracy={:0.4f}'.format(accuracy))
            plt.title(f"Confusion Matrix{self.dataset_name}")
            xpresso_save_plot(f"Confusion_matrix{self.dataset_name}",
                              output_path=PLOTS_FOLDER)
            self.logger.info("Confusion Matrix Saved")
        except Exception as exp:
            self.logger.exception(f"Unable to plot confusion matrix. Reason: "
                                  f"{exp}")

    @ignore_warnings(category=ConvergenceWarning)
    def plot_validation_curves(self, x_values, y_values,
                               param_name="max_iter",
                               param_range=None, scoring_metrics=None,
                               **kwargs):
        """
        Generate validation plots: the test, train scores vs param_name plot
        While default parameters (param_name) for the plot is max_iter,
        users have the options to plot validation curve for any parameters of
        the estimator provided.
        Args:
            x_values : array-like, shape (n_samples, n_features) feature matrix
            y_values : array-like, shape (n_samples) or (n_samples, n_classes)
                target variable
            param_name : str, any parameter of the estimator the user wants
                to evaluate default value is max_iter
            param_range : list, range of parameter values default value is
                range[1,20]
            scoring_metrics: str or list of str, scoring parameters
                pre-defined in sklearn, e.g.,"accuracy" see section "The
                scoring parameter: defining model evaluation rules" within
                sklearn documentation default for regressor is
                "neg_mean_squared_error", "neg_root_mean_squared_error"
                default for classifier is "accuracy", "neg_log_loss"
            **kwargs includes: n_jobs , cv , pre_dispatch , verbose, error_score
                n_jobs-default None, optional
                cv-default 5,  optional
                pre_dispatch-default None, optional
                verbose-int, default=0
                error_score-‘raise’ or numeric, default=np.nan
        """

        # use default metrics if no values provided to scoring_metrics by
        # end-users
        if not scoring_metrics:
            if sklearn.base.is_regressor(self.model):
                scoring_metrics = ["neg_mean_absolute_error",
                                   "neg_root_mean_squared_error"]
            elif sklearn.base.is_classifier(self.model):
                scoring_metrics = ["accuracy", "neg_log_loss"]

        # if end-users provide only one metrics
        elif isinstance(scoring_metrics, str):
            scoring_metrics = [scoring_metrics]

        # if no param_range is provided, param_range is list of integers
        # range from 1 to 20 for max_iter
        if param_range is None:
            param_range = list(range(1, 21))

        fig, axes = plt.subplots(len(scoring_metrics), 1, figsize=(6, 8))

        # plot learning curve for each scoring metrics
        for index, scoring_metric in enumerate(scoring_metrics):
            try:

                train_scores, test_scores = validation_curve(
                    estimator=self.model, X=x_values, y=y_values,
                    param_name=param_name, param_range=param_range,
                    scoring=scoring_metric, **kwargs)

                train_scores_mean = np.mean(train_scores, axis=1)
                train_scores_std = np.std(train_scores, axis=1)
                val_scores_mean = np.mean(test_scores, axis=1)
                val_scores_std = np.std(test_scores, axis=1)

                cv_results_df = pd.DataFrame(
                    {param_name: param_range,
                     "val_scores_mean": val_scores_mean,
                     "train_scores_mean": train_scores_mean})
                val_score_best = val_scores_mean.max()
                max_iters_best = cv_results_df.loc[
                    cv_results_df.val_scores_mean == val_score_best,
                    param_name].values[0]

                # Plot score vs max_iters
                axes[index].grid()
                axes[index].fill_between(param_range,
                                         train_scores_mean - train_scores_std,
                                         train_scores_mean + train_scores_std,
                                         alpha=0.1, color="y")
                axes[index].fill_between(param_range,
                                         val_scores_mean - val_scores_std,
                                         val_scores_mean + val_scores_std,
                                         alpha=0.1, color="g")
                axes[index].plot(param_range, train_scores_mean, 'o-',
                                 color="y",
                                 label="Training score")
                axes[index].plot(param_range, val_scores_mean, 'o-', color="g",
                                 label="Cross-validation score")
                axes[index].plot(max_iters_best, val_score_best, 'r*',
                                 label='Best Param : ' + str(max_iters_best))
                axes[index].set_xlabel(param_name)
                axes[index].set_ylabel(scoring_metrics[index])
                axes[index].set_title(f"Validation curve : Train/Val Scores vs."
                                      f" {param_name}")
                axes[index].legend(loc="best")
            except Exception:
                self.logger.exception("Unable to plot validation curves")
        fig.tight_layout(pad=3.0)
        xpresso_save_plot(f"validation_curve_{self.model.__class__.__name__}",
                          output_path=PLOTS_FOLDER)
        self.logger.info("Learning curve plot saved")


class SklearnMetrics:
    def __init__(self, model, parameters_json=dict):
        self.target_type = EMPTY_STRING
        self.model_type = EMPTY_STRING
        self.model = model
        self.metric_suffix = EMPTY_STRING
        self.calculated_metrics = dict()
        self.logger = XprLogger()
        self.prediction_dict = dict()
        # create a folder to save images or csv files generated from calling
        # report metrics method
        os.makedirs(METRICS_FOLDER, exist_ok=True)
        self.params_json = self.fetch_metric_parameters(parameters_json)

    def validate_model_type(self):
        if not isinstance(self.model, BaseEstimator):
            self.logger.error("Model not built or invalid type")
            return False
        return True

    def set_model_type(self, y_values, model_type=EMPTY_STRING):
        if model_type and model_type.lower() in [REGRESSOR, CLASSIFIER]:
            self.model_type = model_type
        elif is_regressor(self.model):
            self.model_type = REGRESSOR
        elif is_classifier(self.model):
            self.model_type = CLASSIFIER
        elif self.model.__class__.__name__ in sklearn.cluster.__all__:
            self.model_type = UNSUPERVISED_CLUSTERING

    def set_target_type(self, y_values):
        self.target_type = multiclass.type_of_target(y_values)
        try:
            model_classes = self.model.classes_
        except AttributeError:
            model_classes = multiclass.unique_labels(y_values)
        if self.target_type != BINARY and len(model_classes) == 2:
            self.target_type = BINARY

    @staticmethod
    def fetch_metric_parameters(parameters_json):
        parameter_default_value = {
            MODEL_TYPE_ARG: EMPTY_STRING,
            VALIDATION_SIZE_ARG: DEFAULT_VALIDATION_SIZE,
            GENERATE_VALIDATION_METRICS_ARG: False,
            PREDICTION_THRESHOLD_ARG: DEFAULT_PREDICTION_THRESHOLD}
        fetched_parameters = dict()
        for parameter, default_value in parameter_default_value:
            fetched_parameters[parameter] = parameters_json.get(parameter,
                                                                default_value)
        return fetched_parameters

    def get_all_metrics(self, x_values, y_values):
        if not self.validate_model_type():
            return dict()

        if self.params_json.get(GENERATE_VALIDATION_METRICS_ARG):
            try:
                x_train, x_val, y_train, y_val = \
                    train_test_split(
                        x_values, y_values,
                        test_size=self.params_json.get(VALIDATION_SIZE_ARG),
                        random_state=RANDOM_STATE)
                print(f"Generated validation set with validation size:"
                      f"{self.params_json.get(VALIDATION_SIZE_ARG)}, random "
                      f"state:{RANDOM_STATE}")

                # report metrics for training and validation set separately
                for x_data, y_data, dataset_name in [
                    (x_train, y_train, " (Tr)"), (x_val, y_val, " (Val)")]:
                    # make prediction and report metrics
                    self.predict_and_evaluate(
                        x_data, y_data,
                        prediction_threshold=self.params_json.get(
                            PREDICTION_THRESHOLD_ARG),
                        dataset_name=dataset_name,
                        model_type=self.params_json.get(MODEL_TYPE_ARG))
            except Exception:
                generate_validation_metrics = False
                self.logger.exception(
                    "Failed to split dataset into train and validation")
        if not self.params_json.get(GENERATE_VALIDATION_METRICS_ARG):
            self.predict_and_evaluate(
                x_values, y_values, prediction_threshold=self.params_json.get(
                    PREDICTION_THRESHOLD_ARG),
                model_type=self.params_json.get(MODEL_TYPE_ARG))

        return self.calculated_metrics

    def predict_and_evaluate(self, x_values, y_values, prediction_threshold,
                             dataset_name=EMPTY_STRING,
                             model_type=EMPTY_STRING):
        """
        Predict using self.model and calculate the metrics
        Args:
            x_values: array_like or sparse matrix, shape (n_samples, n_features)
                input matrix (either training set, validation set, or test set)
            y_values: array-like of shape (n_samples,) or (n_samples, n_classes)
                target variable vector or matrix (either training set,
                validation set, or test set)
            dataset_name: string (default "")
                The value will be "tr" and "val", indicating training set,
                validation set, if generate_validation_metrics
                is True.
            model_type : String (Optinal) ["regressor", "classifier"] If no
                value is given, the model type will be inferred from model
                loss or the data type of target variable (y) using
                sklearn.utils.multiclass.type_of_target method.
            prediction_threshold : float (default = 0.5) Prediction threshold
                that is compared to prediction probability output by tensorflow
                model.predict() when determining the prediction label for
                classification model.
        """
        self.metric_suffix = dataset_name

        # identify model_type
        self.set_model_type(y_values, model_type)

        # identify target_type
        self.set_target_type(y_values)

        # make predictions
        y_pred, y_pred_proba = self.generate_prediction(x_values, y_values)
        y_score = self.get_y_score(x_values)

        # populate metrics
        self.populate_metrics(x_values, y_pred, y_values, y_pred_proba, y_score)

    def get_y_score(self, x_values):
        """Generate y-score"""
        y_score = None
        if hasattr(self.model, "decision_function"):
            y_score = self.model.decision_function(x_values)
        return y_score

    def generate_prediction(self, x_values, y_values):
        """
        Generate prediction using model.predict()
        Returns: The output from running sklearn model.predict() (labeled as
        y_pred). For sklearn classification models that have
        "decision_function" or "predict_proba" methods, generate predicted
        probabilities or predicted decisions
        """
        print("Generating predictions for given dataset")
        # Apply trained model to make predictions on given dataset
        y_pred = self.model.predict(x_values)
        y_pred_proba = None
        # Check if the trained model can predict probability
        if hasattr(self.model, "predict_proba"):
            y_pred_proba = self.model.predict_proba(x_values)
        return y_pred, y_pred_proba

    def populate_metrics(self, x_values, y_true, y_pred, y_pred_proba, y_score):
        """
        Returns: populate metrics based on model type (regressor, classifier,
        or unsupervised clustering)
        """
        print("Populating metrics")
        # check if the model is sklearn regressor
        if self.model_type == REGRESSOR:
            self.populate_metrics_regressor(y_true, y_pred)
        # check if the model is a sklearn classifier
        elif self.model_type == CLASSIFIER:
            self.populate_metrics_classifier(y_true, y_pred,
                                             y_pred_proba, y_score)
        # check if the model is a unsupervised clustering algorithm within
        # sklearn.cluster module
        elif self.model_type == UNSUPERVISED_CLUSTERING:
            self.populate_metrics_unsupervised_clustering(
                x_values, y_pred, y_true)

    def calculate_metrics(self, metrics_list, value_1, value_2,
                          avg_method=None):
        """
        Calculates the value for list of metrics with given data.
        Args:
             metrics_list: list of sklearn metrics
             value_1: y_true data
             value_2: y_pred data
             avg_method(str optional): avg_method to use for metric evaluation
        """
        for metric in metrics_list:
            key_val = f"{self.metric_suffix} {format_metric_key(metric)}"
            try:
                if not avg_method:
                    self.calculated_metrics[key_val] = round(
                        metric(value_1, value_2), 4)
                    continue
                self.calculated_metrics[f'{key_val}_{avg_method}'] = round(
                    metric(value_1, value_2, average=avg_method), 4)
            except Exception as exp:
                self.logger.exception(
                    f"Unable to calculate metric {metric}. Reason:{str(exp)}")

    def populate_metrics_regressor(self, y_true, y_pred):
        """
        Populate metrics for regression models using sklearn metrics APIs
        Returns: metrics values and metrics name
        """
        print("Populating regressor metrics")
        self.calculate_metrics(SKLEARN_REGRESSOR_METRICS, y_true, y_pred)

    def populate_metrics_classifier(self, y_true, y_pred, y_pred_proba,
                                    y_score):
        """
        Populate metrics, save classification report (as csv file) and plot
        confusion matrix, roc curve and
        precision_recall curve for all three type of classifiers ((binary,
        multiclass or multilabel).
        SKlearnComponent leverage metrics available from sklearn library. The
        metrics that will be
        finally reported depends on the classifier type (binary, multiclass
        or multilable)
        Returns: metrics values and metrics name
        """
        print("Populating classifier metrics")
        if self.target_type not in CLASSIFIER_TARGET_TYPES:
            return

        # save classification report and plots for classifier
        self.save_classification_report(y_true, y_pred)
        self.save_classification_plots(y_true, y_pred, y_pred_proba)

        if y_score:
            self.calculate_metrics([hinge_loss], y_true, y_score)
        if y_pred_proba is not None:
            self.calculate_metrics([log_loss], y_true, y_pred_proba)

        if self.target_type == BINARY:
            self.populate_metrics_binary(y_true, y_pred, y_pred_proba)
        # for multilabel classifier
        elif (self.target_type in MULTI_INDICATOR and
              multiclass.is_multilabel(y_true)):
            self.populate_metrics_multilabel(
                y_true, y_pred, y_pred_proba, y_score)
        # for multiclass classification
        elif self.target_type in MULTI_INDICATOR:
            self.populate_metrics_multiclass(y_pred, y_true, y_pred_proba)

    def populate_metrics_binary(self, y_true, y_pred, y_pred_proba):
        """
        Populate metrics for binary classifiers using both sklearn library.
        Returns: metrics values and metrics name
        """
        self.calculate_metrics(SKLEARN_BINARY_CLASSIFIER_METRICS, y_true,
                               y_pred)

        try:
            true_negative, false_positive, false_negative, true_positive = \
                confusion_matrix(y_true, y_pred).ravel()
            temp_metrics = {
                f"true_negative_rate_{self.metric_suffix}":
                    true_negative_rate(true_negative, false_positive),
                f"false_positive_rate_{self.metric_suffix}":
                    false_positive_rate(false_positive, true_negative),
                f"false_negative_rate_{self.metric_suffix}":
                    false_negative_rate(false_negative, true_positive)}
            self.calculated_metrics.update(temp_metrics)
        except Exception:
            self.logger.exception("Unable to calculate confusion matrix metric")

        # calculate metrics that use prediction probabilities as inputs
        if y_pred_proba is None:
            return

        self.calculate_metrics(
            metrics_list=SKLEARN_CLASSIFIER_PROBABILITY_METRICS,
            value_1=y_true, value_2=y_pred_proba[:, 1])
        try:
            fpr, tpr, _ = roc_curve(y_true, y_pred_proba[:, 1])
            self.calculate_metrics([auc], value_1=fpr,
                                   value_2=tpr)
        except Exception as exp:
            self.logger.exception(
                f"Unable to calculate roc_curve. Reason:{exp}")

    def populate_metrics_multiclass(self, y_true, y_pred, y_pred_proba):
        """
        Populate metrics for multiclass classifiers using both sklearn library.
        Returns: metrics values and metrics name
        """
        self.calculate_metrics(SKLEARN_MULTICLASS_CLASSIFIER_METRICS,
                               y_true, y_pred)
        for avg_method in ["micro", "macro", "weighted"]:
            self.calculate_metrics(SKLEARN_MULTICLASS_AVERAGING_METRICS, y_true,
                                   y_pred, avg_method=avg_method)

        if y_pred_proba is None:
            return
        for avg_method, config in itertools.product(["macro", "weighted"],
                                                    ["ovr", "ovo"]):
            try:
                self.calculated_metrics[
                    f"{format_metric_key(roc_auc_score)}_" \
                    f"{avg_method}_{config}{self.metric_suffix}"] = round(
                    roc_auc_score(y_true, y_pred_proba,
                                  average=avg_method,
                                  multi_class=config), 4)
            except Exception:
                self.logger.exception(f"Unable to calculate roc_aur_score "
                                      f"AvgMethod:{avg_method}, Config:"
                                      f"{config}")

    def populate_metrics_multilabel(self, y_true, y_pred, y_pred_proba,
                                    y_score):
        """
        Populate metrics for multilabel classifiers with sklearn library
        Returns: metrics values and metrics name
        """
        if not y_score:
            y_score = y_pred_proba
        self.calculate_metrics(SKLEARN_MULTILABEL_CLASSIFIER, y_pred, y_true)

        for avg_method in ["micro", "macro", "weighted", "samples"]:
            self.calculate_metrics(SKLEARN_MULTILABEL_AVERAGING_METRICS, y_true,
                                   y_pred, avg_method)

        self.calculate_metrics(SKLEARN_MULTILABEL_RANKING_METRICS,
                               y_true, y_score)

        if y_pred_proba is None:
            return

        # reshape prediction probability
        y_pred_proba_t = np.transpose(np.array(y_pred_proba)[:, :, 1])

        # roc_auc_score for multilabel
        for avg_method in ["micro", "macro", "weighted", "samples"]:
            try:
                self.calculated_metrics[
                    f"{format_metric_key(roc_auc_score)}_" \
                    f"{avg_method}{self.metric_suffix}"] = round(
                    roc_auc_score(y_true, y_pred_proba_t,
                                  average=avg_method), 4)
            except Exception as exp:
                self.logger.exception(f"Unable to calculate roc_auc_score "
                                      f"AvgMethod:{avg_method}. reason: {exp}")

    def populate_metrics_unsupervised_clustering(self, x_values, y_pred,
                                                 y_true):
        """ Populate metrics for unsupervised clustering model"""
        self.calculate_metrics(SKLEARN_UNSUPERVISED_CLUSTERING_METRICS,
                               y_pred, y_true)

        # additional metrics for unsupervised clustering models
        self.calculate_metrics(SKLEARN_CLUSTERING_SPECIFIC_METRICS,
                               x_values, y_pred)

    def save_classification_plots(self, y_true, y_pred, y_pred_proba):
        print(f"Saving classification plots to {PLOTS_FOLDER}")
        sklearn_plot_utils = SklearnPlots(self.model, self.metric_suffix)
        sklearn_plot_utils.set_model_classes(y_true)
        if self.target_type == BINARY:
            sklearn_plot_utils.plot_confusion_matrix(y_true, y_pred)
            if y_pred_proba is None:
                return
            sklearn_plot_utils.plot_roc_curve_binary(y_true, y_pred_proba)
            sklearn_plot_utils.plot_precision_recall_curve_binary(
                y_true, y_pred_proba)
        elif multiclass.unique_labels(y_true).size > 2 or \
            self.target_type in MULTI_INDICATOR:
            sklearn_plot_utils.plot_precision_recall_curve_multi(
                y_true, y_pred_proba)
            sklearn_plot_utils.plot_roc_curve_multi(y_true, y_pred_proba)
            if multiclass.is_multilabel(y_true):
                sklearn_plot_utils.plot_multilabel_confusion_matrix(
                    y_true, y_pred)
            else:
                sklearn_plot_utils.plot_confusion_matrix(y_true, y_pred)

    def save_classification_report(self, y_true, y_pred):
        """
        Save classification report(as csv file) for all types of
        classification models
        """
        try:
            cls_report = classification_report(y_true, y_pred, output_dict=True)
            cls_report_df = pd.DataFrame(cls_report).transpose()
            # save model to data folder
            pickle.dump(cls_report_df, open(os.path.join(
                METRICS_FOLDER, f"classification_report"
                                f"_{self.metric_suffix}.pkl"), 'wb'))
            self.logger.info("Classification Report Saved")
        except Exception as exp:
            self.logger.exception(f"Unable to save classification report. "
                                  f"Reason: {exp}")
